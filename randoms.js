function getIntRandomInRange(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}

let numPool = [];
let i;
for (i=0; i<301; i++) {
    numPool[i] = 0;
}
let maxIteration = 50000;
let tempNumber;
let min = 0;
let max = 300;
for (i=0; i<maxIteration; i++) {
    tempNumber = ((getIntRandomInRange(min, max)+getIntRandomInRange(min, max)+getIntRandomInRange(min, max))/3);
    numPool[tempNumber]++;
}
for (i=0; i<301; i++) {
    document.writeln("<p>" + i + " " + numPool[i] + "</p>");
}
let canvas = document.getElementById('tutorialPDF');
if (canvas.getContext)
{
    let ctx = canvas.getContext('2d');
    let x0 = 0;
    let y0 = 200;
    for (i=0; i<max+1; i++)
    {
        ctx.beginPath();
        ctx.moveTo(x0+i, y0);
        ctx.lineTo(x0+i, y0-numPool[i]);
        ctx.closePath();
        ctx.stroke();
    }
}