function getIntRandomInRange(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}
function getIntRandomQNormal(min, max) {
    return (getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max))/10;
}
let canvas = document.getElementById('BrownMove');
let ctx = canvas.getContext('2d');
const X = canvas.width;
const Y = canvas.height;
const x0 = X/2;
const y0 = Y/2;
const R = 90;
const minStep = -100;
const maxStep = 100;
ctx.fillStyle = "#F2AA00";
let raf;
let x1, x2;
let particle = {
    x: x0,
    y: y0,
    dx: 0,
    dy: 0,
    radius: R,
    draw: function() {
        ctx.beginPath();
        ctx.arc(this.x,this.y, this.radius, 0, Math.PI*2);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
};
function draw() {
    ctx.clearRect(0, 0, X, Y);
    particle.draw();
    x1 =getIntRandomQNormal(minStep, maxStep);
    x2 =getIntRandomQNormal(minStep, maxStep);
    //x1 =getIntRandomInRange(minStep, maxStep);
    //x2 =getIntRandomInRange(minStep, maxStep);
    particle.x += x1;
    particle.y += x2;
    if (particle.y > canvas.width - R || particle.y < R)
    {
        particle.y += -x2;
    }
    if (particle.x  > canvas.height - R || particle.x < R)
    {
        particle.x += -x1;
    }
    raf = window.requestAnimationFrame(draw);
}
canvas.addEventListener('mouseover',function()
        {raf=window.requestAnimationFrame(draw);
canvas.addEventListener('mouseout',function(){window.cancelAnimationFrame(raf);});
        particle.draw();});