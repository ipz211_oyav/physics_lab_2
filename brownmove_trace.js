function getIntRandomInRange(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}
function getIntRandomQNormal(min, max) {
    return (getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max) +
        getIntRandomInRange(min, max))/10;
}

let canvas = document.getElementById("BrownMove");
let ctx = canvas.getContext("2d");
const X = canvas.width;
const Y = canvas.height;
const x0 = X/2;
const y0 = Y/2;
const R = 20;
const minStep = -39;
const maxStep = 39;
ctx.fillStyle = "#F308C4";
let dx = 0, dy = 0;
let x = x0, y = y0;
let i = 0;
do {
    ctx.beginPath();
    ctx.arc(x, y, R, 0, 2*Math.PI);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
    dx = getIntRandomInRange(minStep, maxStep);
    dy = getIntRandomInRange(minStep, maxStep);
    //dx = getIntRandomQNormal(minStep, maxStep);
    //dy = getIntRandomQNormal(minStep, maxStep);
    x = x + dx;
    y = y + dy;
    if (y > canvas.width - R || y < R)
    {
        y += -dy;
    }
    if (x > canvas.height - R || x < R)
    {
        x += -dx;
    }
    i++;
} while (i<100);